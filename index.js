import { createRequire } from "module";
const require = createRequire(import.meta.url);
const cors = require('cors');

import express from "express";
var bodyParser = require('body-parser');
const multer = require('multer')
var forms = multer();
const multerS3 = require('multer-s3')
let fs = require('fs')


import {
    S3Client,
    ListBucketsCommand,
    ListObjectsV2Command,
    GetObjectCommand,
    PutObjectCommand
} from "@aws-sdk/client-s3";


const ACCOUNT_ID = '353b244ce35f10ba163ed01a235a1aaa'
const ACCESS_KEY_ID = '62eeb0baf527ab79c46348caf0faac65'
const SECRET_ACCESS_KEY = '31237fe776a2396ba68c5c28a9595564ca895cb31ae36ac8ec4693fc4ebb0191'

const corsOptions = {
    origin: 'https://localhost:5015', // Allow requests only from this origin
    methods: 'GET,HEAD,PUT,PATCH,POST,DELETE', // Allow specific HTTP methods
    optionsSuccessStatus: 204, // Set the status code for successful preflight requests
  };
  

const app = express();
const port = 3000;

let s3 = new S3Client({
    region: "auto",
    endpoint: `https://${ACCOUNT_ID}.r2.cloudflarestorage.com`,
    credentials: {
        accessKeyId: ACCESS_KEY_ID,
        secretAccessKey: SECRET_ACCESS_KEY,
    },
});

const upload = multer({
    storage: multerS3({
        s3: s3,
        bucket: 'tydev',
        contentType: multerS3.AUTO_CONTENT_TYPE,
        key: function (req, file, cb) {
            console.log(file);
            cb(null, file.originalname); //use Date.now() for unique file keys
        }
    })
})



// console.log(await S3.send(new ListObjectsV2Command({ Bucket: "tydev" })));
app.use(cors(corsOptions));

// RESTFULL API
app.get('/', (req, res) => {
    res.send('Welcome to my server!');
});
app.post('/post-object', upload.single('test'), async (req, res, next) => {


    // // The name of the input field (i.e. "sampleFile") is used to retrieve the uploaded file
    // var file22 = req.body
    // var contentle = +req.headers['content-length']
    // var contentty = req.headers['content-type']
    // let S3 = new S3Client({
    //     region: "auto",
    //     endpoint: `https://${ACCOUNT_ID}.r2.cloudflarestorage.com`,
    //     credentials: {
    //         accessKeyId: ACCESS_KEY_ID,
    //         secretAccessKey: SECRET_ACCESS_KEY,
    //     },
    // });
    // const readableStream = await fs.createReadStream(req.files[0].buffer);



    // const putCommand = new PutObjectCommand({
    //     Bucket: 'tydev',
    //     Key: req.files[0].originalname,
    //     ContentLength: contentle,
    //     Body: readableStream
    // })

    // const response = await S3.send(putCommand);


    console.log(req.body);
    console.log(req.files);
    res.send('Successfully uploaded ' + req.files.length + ' files!')
});


app.post('/upload', upload.array('file'), function (req, res, next) {
    console.log("YOH")
    res.send({
        // message: "Uploaded!",
        // urls: req.files.map(function(file) {
        //     return {url: file.location, name: file.key, type: file.mimetype, size: file.size};
        // })
    });
})

app.listen(port, () => {
    console.log(`Server is running on port ${port}`);
});
